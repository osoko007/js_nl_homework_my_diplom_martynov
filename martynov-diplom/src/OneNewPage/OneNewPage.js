import React from 'react';
import '../MyStyle/MyStyle.scss';

const NewsItemLink = props => {
    /*getting last word on title for link*/
    const newsName = props.location.state[0].title;
    const lastWordArr = newsName.split(' ');
    const lastWordArrLast = lastWordArr.splice(0,lastWordArr.length-1);
    const lastWordArrLastString = lastWordArrLast.join(' ')
    const lastWord = lastWordArr[lastWordArr.length-1];
    /*Getting day and mounth*/
    const publishData = new Date(props.location.state[0].publishedAt);
    const getNum = data => data < 10 ? '0' + data : data;
    const day = getNum(publishData.getDate());
    const mounth = getNum(publishData.getMonth() + 1);

    return (
        <div style={{display: 'flex', justifyContent:'space-between'}}>
            <div>
                <h2 className="link-h2">
                    <span>
                        {lastWordArrLastString} 
                    </span>
                    <a className="linkToNews" href={props.location.state[0].url}> {lastWord}</a>
                </h2>
                <div className="span-name-agent">
                    <span className="agent-span">
                        {props.location.state[0].source.name}
                    </span>
                </div>
                <div>
                    <span className="data-day">{day} / </span>
                    <span className="data-slash"> / </span>
                    <span className="data-mounth">{mounth}</span>
                </div>
                
            </div>
            <div style={{width: 730}}>
                <img style ={{width: '100%'}} src={props.location.state[0].urlToImage}></img>
                <p className="news-text">{props.location.state[0].description}</p>
            </div>
        </div>

    )
}

export default NewsItemLink;
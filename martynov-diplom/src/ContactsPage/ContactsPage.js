import React from 'react';
import Avatar from '../Y6HOCQD5Doc.jpg';

const ContactsPage = () => {
    return (
        <div className="offer-contact">
            <div className="contact-offer-left">
                <h1 className="title-tel">
                    + 7 (968) 760 77 70
                </h1>
                <p className="my-full-name">Павел Мартынов</p>
                <p className="my-email">osoko007@gmail.com</p>
                <p className="prof">JavaScript разработчик</p>
                <p className="knowns">ES5, ES6, <span><a style={{textDecoration:'none', color:'#004FEC'}} href="https://reactjs.org/">React</a></span></p>
            </div>
            <div style={{width:540}}>
                <img style={{width:'100%'}} src={Avatar}></img>
            </div>
        </div>
    )
}

export default ContactsPage;
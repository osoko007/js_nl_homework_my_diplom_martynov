import React from 'react';
import NewsItem from '../NewsItem/NewsItem';
import '../MyStyle/MyStyle.scss';
import {NavLink} from 'react-router-dom';

const HomePage = props => {
    const result = props.props;
    console.log(result);
    return (
        <div className="newsList container">
            <div className="offer-title">
                <h1 className="title">Всегда</h1>
                <div style ={{margin: 0, padding: 0}}>
                    <span className="title-span">свежие</span>
                    <NavLink className="title-link" to="/News">новости</NavLink>
                </div> 
            </div>
            <div className="flex-wrap">
                {
                    result && result.articles.map((item,index) => {
                        return index < 6? <NewsItem
                                    title = {item.title}
                                    key = {index}
                                    newsName = {item.source.name}
                                    props = {item}
                                    publishedAt = {item.publishedAt}
                                /> : null;
                    })
                }
            </div>
            <div className="padding-link-to">
                <NavLink className="link-to" to="/News">Быть в курсе событий</NavLink>
            </div>
        </div>  
    )
}

export default HomePage;
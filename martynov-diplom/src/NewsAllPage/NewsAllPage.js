import React from 'react';
import NewsItem from '../NewsItem/NewsItem';
import '../MyStyle/MyStyle.scss';
import {NavLink} from 'react-router-dom';

const NewsAllPage = props => {
    const result = props.props;
    return (
        <div className="newsList container">
            <div className="offer-title">
                <h1 className="title">Быть</h1>
                <div style ={{margin: 0, padding: 0}}>
                    <span className="title-span">в курсе</span>
                    <NavLink className="title-link" to="/News">событий</NavLink>
                </div>
                
            </div>
            <div className="flex-wrap">
                {
                    result && result.articles.map((item,index) => {
                        return <NewsItem
                                    title = {item.title}
                                    key = {index}
                                    newsName = {item.source.name}
                                    props = {item}
                                    publishedAt = {item.publishedAt}
                                />
                    })
                }
            </div>
        </div>
    )
}

export default NewsAllPage;
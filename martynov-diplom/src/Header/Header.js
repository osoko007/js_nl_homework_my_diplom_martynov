import React from 'react';
import {NavLink} from 'react-router-dom';
import '../MyStyle/MyStyle.scss';

const Header = () => {
    return(
        <header className='header'>
        <div>
          <nav className="menu">
            <div className="logo-div">
              <span className="logo">
                Новостник
              </span>
            </div>
            <ul>
              <NavLink exact activeClassName="active-link" className="link" to="/">Главная</NavLink>
              <NavLink exact activeClassName="active-link" className="link" to="/News">Новости</NavLink>
              <NavLink exact activeClassName="active-link" className="link" to="/Contacts">Контакты</NavLink>
            </ul>
          </nav>  
      </div>
      </header>
    )
}

export default Header;
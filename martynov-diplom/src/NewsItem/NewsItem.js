import React from 'react';
import '../MyStyle/MyStyle.scss';
import {withRouter} from 'react-router-dom';


const NewsItem = props => {
    /*Тут пришлось помучаться, чтобы убрать символы в конце или начале или середине слова, а так же символы между словами*/  
    const urlTitle = props.props.title.split(' ');
    let arr = [];
    urlTitle.forEach((item,index)=>{
        let arr2 = item.split('');
        let arrWord = [];
        arr2.forEach((item,index)=>{
            if(item !== ':' && item !=='.' && item !==',' && item !=='_' && item !=='/' && item !=='-' && item !==' ' && item !=='–') {
                arrWord.push(item);
            }
        });
        let strWord = arrWord.join('')
        if(item !== ':' && item !=='.' && item !==',' && item !=='_' && item !=='/' && item !=='-' && item !==' ' && item !=='–') {
            arr.push(strWord);
        }
    })
    /*Получаю url без лишних символов с разделением "-"*/
    const newUrlTitle = arr.join('-');
    
    
    const func = props => {
        props.history.push('/News/' + newUrlTitle, [props.props]);
    }
    /*Getting day and mounth*/
    const publishData = new Date(props.publishedAt);
    const getNum = data => data < 10 ? '0' + data : data;
    const day = getNum(publishData.getDate());
    const mounth = getNum(publishData.getMonth() + 1);

    return (
        <div style={{marginRight:25, marginTop:40}}>
            <div className="one-new" onClick={()=>func(props)}>
                <div>
                    <p className="text-one-new">{props.title}</p>
                </div>
            </div>
            <div className="data-offer">
                <div>
                    <span className="agent-span">{props.newsName}</span>
                </div>
                <div>
                    <span className="data-day">{day}</span>
                    <span className="data-slash"> / </span>
                    <span   span className="data-mounth">{mounth}</span>
                </div>
            </div>
        </div>
        
    )
}

export default withRouter(NewsItem);
import React, {useState, useEffect} from 'react';
import './App.css';
import './MyStyle/MyStyle.scss';
import {Route} from 'react-router-dom';
import NewsAllPage from './NewsAllPage/NewsAllPage';
import HomePage from './HomePage/HomePage';
import ContactsPage from './ContactsPage/ContactsPage';
import OneNewPage from './OneNewPage/OneNewPage';
import axios from 'axios';
import Header from './Header/Header';
import Footer from './Footer/Footer';

const useFetchDataHome = () => {
  const [data, setData] = useState(null);
  var testUrl = 'http://newsapi.org/v2/top-headlines?' +
  'country=ru&' +
  'apiKey=a08a29f911054a4d8a3cdcd3b4a28ede';
  var req = new Request(testUrl);
  useEffect(() => {
      const fetchData = async () => {
          const response = await axios.get(testUrl);
          setData(response.data);
      };
      fetchData();
  }, []);
  return data;
}

function App() {
  const result = useFetchDataHome();
  return (
      <div className="App container">
      <Header/>
      <section className='main'>
        <Route
          path="/"
          exact
          render={()=><HomePage props={result}/>}
        />
        <Route
          path="/News"
          exact
          render={()=><NewsAllPage props={result}/>}
        />
        <Route
          path="/Contacts"
          exact
          component={ContactsPage}
        />
        <Route
          path="/News/:name"
          exact
          component={OneNewPage}
        />
      </section>
      <Footer/>
    </div>
  );
}

export default App;

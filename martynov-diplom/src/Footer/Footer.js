import React from 'react';

const Footer = () => {
    return (
        <footer className='footer-offer'>
        <div className="footer">
          <div className="footer-logo">
            <span className="logo">
              Новостник
            </span>
            <span className="spa">
              Simple Page Application 
            </span>
          </div>
          <div>
            <p style={{fontSize :15}}>
              Дипломный проект
            </p>
          </div>
          <div>
            <span style={{display:"block", textAlign : "right"}}>
              Made by
            </span>
            <span className="logo">
              Павел Мартынов
            </span>
          </div>
        </div>
      </footer>   
    )
}

export default Footer;